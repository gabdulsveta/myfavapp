import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  { path: '', redirectTo: '/recipes', pathMatch: 'full' },

  // lazyloading - вместо компонента, указывается путь к модулю и через # имя модуля
  { path: 'recipes', loadChildren: './recipes/recpies.module#RecipesModule' },
  { path: 'shopping-list', loadChildren: './shopping-list/shopping-list.module#ShoppingListModule' },
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})
    //  {preloadingStrategy: PreloadAllModules} для предварительной загрузки в lazyloading
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {

}
