import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';

import * as ShoppingListActions from './store/shopping-list.actions';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  // ingredients: Ingredient[];
  ingredients: Observable<{ ingredients: Ingredient[] }>;
  // private igChangeSub: Subscription;
  constructor(
    private store: Store<fromApp.AppState>
  ) { }

  ngOnInit() {

    // rxjs Observable
    this.ingredients = this.store.select('shoppingList');

    // или подписаться
    this.store.select('shoppingList').subscribe();

    // this.ingredients = this.shoppingListService.getIngredients();

    // this.igChangeSub = this.shoppingListService.ingredientsChanged
    //   .subscribe(
    //     (ingredients: Ingredient[]) => {
    //       this.ingredients = ingredients;

    //     }
    //   );
  }
  onEditItem(index: number) {
    // this.shoppingListService.startedEditing.next(index);
    this.store.dispatch(new ShoppingListActions.StartEdit(index));
  }
  ngOnDestroy() {
    // this.igChangeSub.unsubscribe();
  }
}
