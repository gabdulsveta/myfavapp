import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpParams } from '@angular/common/http';
import { AuthService } from './auth.service';
import { exhaustMap, take, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import * as fromApp from '../store/app.reducer';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private authService: AuthService, private store: Store<fromApp.AppState>) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    return this.store.select('auth').pipe(
  // take -  оператор для один раз подписаться, в скобках указывается число значений(value), после автоматически отписывается
  // возвращает Observable
      take(1),
      map(authSate => {
        return authSate.user;
      }),
  // exhaustMap - оператор ждет выполнения предыдущего tap, получает Data из предыдущего Observable, возвращает новый Observable, который
 // заменяет собой предыдущий.
      exhaustMap(user => {
        if (!user) {
          return next.handle(req);
        }
        // params, чтобы отправить token
        const modifiedReq = req.clone({params: new HttpParams().set('auth', user.token)});
        return next.handle(modifiedReq);
      }));
  }
}
